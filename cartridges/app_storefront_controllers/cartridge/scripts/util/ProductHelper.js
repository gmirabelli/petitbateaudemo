'use strict';

/**
 * @module util/ProductHelper
 */

var QtyLimit = parseInt(dw.system.Site.getCurrent().getCustomPreferenceValue('PB_productQtyLimit'));

const ProductHelper = {
    /**
     * Returns array of "Care" custom objects
     *
     * @param {Array} careCodesArr IDs of "Care" custom objects
     * @returns {Array} custom objects
     */
    getCareCodes: function(careCodesArr) {
        var itemsToShow = [];

        if (careCodesArr && careCodesArr.length > 0) {
            var careCustomObjects = dw.object.CustomObjectMgr.getAllCustomObjects('Care');

            while (careCustomObjects.hasNext()) {
                var customObject = careCustomObjects.next();

                if (careCodesArr.indexOf(customObject.custom.ID) > -1) {
                    itemsToShow.push(customObject);
                }
            }
        }

        return itemsToShow;
    },
    /**
     * Checks if the product quantity limit is reached which can be ordered at once
     * @param {dw.order.ProductLineItem} lineItem
     * @param {Number} qtyToAdd
     * @returns {boolean} isLimitReached
     */
    isQtyLimitReached: function(lineItem, qtyToAdd) {
        var isLimitReached = false;

        if (lineItem !== null) {
            var qty = parseInt(lineItem.quantityValue);
            isLimitReached = qty >= QtyLimit;
        } else {
            isLimitReached = qtyToAdd > QtyLimit;
        }

        return isLimitReached;
    },
    /**
     * Returns QTY Limit of current storefront
     * @returns {Number}
     */
    getQtyLimit: function() {
        return QtyLimit;
    },
    /**
     * Returns HTML of content asset
     * @param {String} contentAssetID
     * @returns {String} html
     */
    renderContentAsset: function(contentAssetID) {
        var html = '';

        if (contentAssetID) {
            var contentAsset = dw.content.ContentMgr.getContent(contentAssetID);

            if (contentAsset) {
                html = contentAsset.custom.body;
            }
        }

        return html;
    },
    /**
     * Loops through given attributes and looks for not empty values
     * @param {dw.catalog.Product} product
     * @param {Array} attrArr
     * @returns {Array} valuesArr
     */
    getCustomAttributeValues: function(product, attrArr) {
        var valuesArr = [];

        for (var i = 0, size = attrArr.length; i < size; i++) {
            var val = product.custom[attrArr[i]];

            if (!empty(val)) {
                valuesArr.push(val);
            }
        }

        return valuesArr;
    }
};

module.exports = ProductHelper;