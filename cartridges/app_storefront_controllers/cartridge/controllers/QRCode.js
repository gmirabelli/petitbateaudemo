'use strict';

/**
 * Controller that provides functions for editing, adding, and removing addresses in a customer addressbook.
 * It also sets the default address in the addressbook.
 * @module controllers/Address
 */

/* API Includes */
var Resource = require('dw/web/Resource');
var Transaction = require('dw/system/Transaction');
var URLUtils = require('dw/web/URLUtils');

/* Script Modules */
var app = require('~/cartridge/scripts/app');
var guard = require('~/cartridge/scripts/guard');

/**
 * Show the QrCode
 */
function show() {
    app.getView().render('content/qrcode/qrcode');
}


/** Lists addresses in the customer profile.
 * @see {@link module:controllers/Address~list} */
exports.Show = guard.ensure(['get'], show);