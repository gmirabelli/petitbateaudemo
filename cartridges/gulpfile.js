'use strict'
var gulp = require('gulp')
var sourcemaps = require('gulp-sourcemaps')
var uglify = require('gulp-uglify')
var cleanCSS = require('gulp-clean-css')
var concat = require('gulp-concat')

var fs = require('fs');
var gif = require('gulp-if')
var merge = require('merge-stream')
var sass = require('gulp-sass')
var autoprefixer = require('gulp-autoprefixer')
var aliasify = require('aliasify')

var plumber = require('gulp-plumber')
var notify = require('gulp-notify')

var browserify = require('browserify')
var buffer = require('vinyl-buffer')
var source = require('vinyl-source-stream')
var watchify = require('watchify')
var xtend = require('xtend')

var iconfont = require('gulp-iconfont');
var runTimestamp = Math.round(Date.now()/1000);
var consolidate = require('gulp-consolidate');
var nodeSass = require('node-sass');

var config = require('./gulp-config')
var paths = config.paths

var watching = false

const metadataConfig = require('./metadata-config.json');

var sassOptions = {
	functions: {
		'encodeBase64($string)': function($string) {
			var SassString = require('node-sass').types.String;
			var fileContent = fs.readFileSync(config.iconfont.static + 'default/' + $string.getValue());
			var base64Encoded = new Buffer(fileContent).toString('base64');
			return new SassString(base64Encoded);
		}
	}
}

function logTime () {
	var date = new Date()
	return '[' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + '] '
}

function js(minify) {
	paths.forEach(function(path) {
		
		if (!path.js) {return}
		
		var opts = {
			entries: path.js.src + 'app.js',
			debug: true
		}
		
		if (watching) {
			opts = xtend(opts, watchify.args)
		}
		
		var bundler = browserify(opts)
		if (watching) {
			bundler = watchify(bundler)
		}
		
		function rebundle () {
			return bundler.transform(aliasify).bundle()
			.on('error', notify.onError('Error: <%= error.message %>'))
			.pipe(source('app.js'))
			// sourcemaps
			.pipe(buffer())
			.pipe(gif(minify, uglify().on('error', function(e){console.log(e)})))
			.pipe(sourcemaps.init({loadMaps: true}))
			.pipe(sourcemaps.write('./'))
			//
			.pipe(gulp.dest(path.js.dest))
		}
		
		bundler.on('update', function (ids) {
			console.log(logTime() + 'File(s) changed: ' + ids)
			console.log(logTime() + 'Rebundling...')
			rebundle()
		})
		
		bundler.on('log', console.log)
		bundler.on('error', notify.onError('Error: <%= error.message %>'))
		return rebundle()
	})
}

function css(minify) {
	paths.forEach( function(path) {
		
		if (!path.css) {return}
		
		gulp.src(path.css.src + '/**/*.scss')
		.pipe(plumber({errorHandler: notify.onError('Error: <%= error.message %>')}))
		.pipe(sourcemaps.init({loadMaps: true}))
		.pipe(sass(sassOptions))
		.pipe(autoprefixer('last 2 versions'))
		.pipe(gif(minify, cleanCSS({compatibility: 'ie8'})))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(path.css.dest))
	})
}

/**
* This is to make font from svgs
*/
gulp.task('icon', function () {
	return gulp.src([config.iconfont.media + '/svgs/**/*.svg'])
	.pipe(iconfont({
		fontName: 'iconfont',
		firstGlyph: true,
		centerHorizontally: true,
		normalize: true,
		prependUnicode: true,
		formats: ['woff'],
		fontHeight: 1001,
		timestamp: runTimestamp, // recommended to get consistent builds when watching files
	})).on('glyphs', (glyphs) => {
		gulp.src(config.iconfont.media + '/svgs/_iconfont.scss')
		.pipe(consolidate('lodash', {
			className: 'fonticos',
			fontName: 'iconfont',
			fontPath: '../../static/default/fonts/iconfont/', // set path to font (from your CSS file if relative)
			glyphs: glyphs.map(mapGlyphs)
		}))
		.pipe(gulp.dest(config.iconfont.static + '/../scss/default/')) // set path to export your CSS
	})
	.pipe(gulp.dest(config.iconfont.static + 'default/fonts/iconfont/')) // set path to export your fonts
});

/**
* This is needed for mapping glyphs and codepoints.
*/
function mapGlyphs(glyph) {
	return {
		name: glyph.name,
		codepoint: glyph.unicode[0].charCodeAt(0).toString(16)
	}
}


gulp.task('css', function() {return css(false)})
gulp.task('js', function() {return js(false)})

gulp.task('js-minify', function() {return js(true)})
gulp.task('css-minify', function() {return css(true)})

gulp.task('build', ['js-minify', 'css-minify'])

gulp.task('dev', ['enable-watch', 'js', 'css'], function () {
	paths.forEach(function(path) {
		if (!path.css) {return}
		gulp.watch(path.css.src + '/**/*.scss', ['css'])
	})
})

gulp.task('default', ['enable-watch', 'js', 'css'], function () {
	paths.forEach(function(path) {
		if (!path.css) {return}
		gulp.watch(path.css.src + '/**/*.scss', ['css'])
	})
})

gulp.task('enable-watch', function() {
	watching = true
})

gulp.task('metadata:split', function () {
	if (!metadataConfig) return console.log('Could not find the configuration file.');
	const metaUtils = require(metadataConfig.rootfile);
	return Promise.all([
		metaUtils.system.split(metadataConfig.systemObjectPath),
		metaUtils.custom.split(metadataConfig.customObjectPath)
	], console.log);
})

gulp.task('metadata:merge', function () {
	if (!metadataConfig) return console.log('Could not find the configuration file.');
	const metaUtils = require(metadataConfig.rootfile);
	return Promise.all([
		metaUtils.system.merge(metadataConfig.systemObjectPath),
		metaUtils.custom.merge(metadataConfig.customObjectPath)
	], console.log);
})

gulp.task('metadata:watch', function() {
	if (!metadataConfig) return console.log('Could not find the configuration file.');
	gulp.watch([
		metadataConfig.systemObjectPath.inputSplitFilesPath + '/**',
		metadataConfig.customObjectPath.inputSplitFilesPath + '/**'
	], {}, ['metadata:merge']);
	
})
