'use strict';

/**
 * @private
 * @function
 * @This function add special data attribute to <body> and fire special event
 * depending on device width 
 * mobile : data-devicetype = "mobile" and event name : "mobileAction"
 * desktop : data-devicetype = "desktop" and event name : "desktopAction"
 * exp: $(document).on('mobileAction', function(event){......})
 * the XXaction_continuous event fire during all resize, contrary to the XXaction which fire only during a change of device
 * the param "winWidth" help us to get the Window width when trigger the event 
 * and we can use the data attribute to check the device type and use different action for each one
 */
var s,
	resizeEvent = {

		settings: {
			v_window : $(window),
			v_body : $('body'),
			v_document : $(document)
		},

		init: function() {
			s = this.settings;
			this.breakActions();
			this.bindResizeEvent();
		},

		breakActions: function() {
			var winWidth = window.innerWidth;
			/* desktop or not */
			if (winWidth >= window.breakpoint.desktop) {
				if (s.v_body.data('devicetype') !== 'desktop') {
					s.v_document.trigger('desktopAction', winWidth);
					s.v_body.data('devicetype','desktop');
				}
				s.v_document.trigger('desktopAction_continuous', winWidth);
			} else if (winWidth < window.breakpoint.desktop) {
				if (s.v_body.data('devicetype') !== 'mobile') {
					s.v_document.trigger('mobileAction', winWidth);
					s.v_body.data('devicetype','mobile');
				}
				s.v_document.trigger('mobileAction_continuous', winWidth);
			}
		},

		bindResizeEvent: function(){
			s.v_window.on('resize', function(){
				resizeEvent.breakActions();
			});
		}

	};
	
	
/* ===== Smartresize ===== */
(function($,sr){

	// debouncing function from John Hann
	// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
	var debounce = function (func, threshold, execAsap) {
		var timeout;

		return function debounced () {
			var obj = this, args = arguments;
			function delayed () {
				if (!execAsap)
					func.apply(obj, args);
				timeout = null;
			}

			if (timeout)
				clearTimeout(timeout);
			else if (execAsap)
				func.apply(obj, args);

			timeout = setTimeout(delayed, threshold || 200);
		};
	}
	
	jQuery.fn[sr] = function(fn){ return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery,'smartresize');

module.exports = resizeEvent;
