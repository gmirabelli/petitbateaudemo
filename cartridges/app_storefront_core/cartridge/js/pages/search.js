'use strict';

var compareWidget = require('../compare-widget'),
	productTile = require('../product-tile'),
	progress = require('../progress'),
	util = require('../util');

function infiniteScroll() {
	// getting the hidden div, which is the placeholder for the next page
	var loadingPlaceHolder = $(
		'.infinite-scroll-placeholder[data-loading-state="unloaded"]'
	);
	// get url hidden in DOM
	var gridUrl = loadingPlaceHolder.attr('data-grid-url');

	if (
		loadingPlaceHolder.length === 1 &&
		util.elementInViewport(loadingPlaceHolder.get(0), 250)
	) {
		// switch state to 'loading'
		// - switches state, so the above selector is only matching once
		// - shows loading indicator
		loadingPlaceHolder.attr('data-loading-state', 'loading');
		loadingPlaceHolder.addClass('infinite-scroll-loading');

		// named wrapper function, which can either be called, if cache is hit, or ajax repsonse is received
		var fillEndlessScrollChunk = function(html) {
			loadingPlaceHolder.removeClass('infinite-scroll-loading');
			loadingPlaceHolder.attr('data-loading-state', 'loaded');
			$('div.search-result-content').append(html);
		};

		// old condition for caching was `'sessionStorage' in window && sessionStorage["scroll-cache_" + gridUrl]`
		// it was removed to temporarily address RAP-2649

		$.ajax({
			type: 'GET',
			dataType: 'html',
			url: gridUrl,
			success: function(response) {
				// put response into cache
				try {
					sessionStorage['scroll-cache_' + gridUrl] = response;
				} catch (e) {
					// nothing to catch in case of out of memory of session storage
					// it will fall back to load via ajax
				}
				// update UI
				fillEndlessScrollChunk(response);
				productTile.init();
			}
		});
	}
}
/**
 * @private
 * @function
 * @description replaces breadcrumbs, lefthand nav and product listing with ajax and puts a loading indicator over the product listing
 */
function updateProductListing(url, format, container) {
	var container = container || '#main'
	if (!url || url === window.location.href) {
		return;
    }
    if($('#viewButton').length > 0) {
        url = util.appendParamToURL(url, 'view', $('#viewButton li.selected').data('view'));
    }
    if (format !== false) {
        url = util.appendParamToURL(url, 'format', typeof format == "undefined" ? 'ajax' : format);
    }
	progress.show($('.search-result-content'));
	$(container).load(url, function() {
        progress.hide();
        initPriceSlider();
        compareWidget.init();
        productTile.init();
        history.pushState(undefined, '', util.removeParamFromURL(url, 'format'));
    });
}

/**
 * @private
 * @function
 * @description Initializes events for the following elements:<br/>
 * <p>refinement blocks</p>
 * <p>updating grid: refinements, pagination, breadcrumb</p>
 * <p>item click</p>
 * <p>sorting changes</p>
 */
function initializeEvents() {
	var $main = $('#main');
	// compare checked
	$main.on('click', 'input[type="checkbox"].compare-check', function() {
		var cb = $(this);
		var tile = cb.closest('.product-tile');

		var func = this.checked
			? compareWidget.addProduct
			: compareWidget.removeProduct;
		var itemImg = tile.find('.product-image a img').first();
		func({
			itemid: tile.data('itemid'),
			uuid: tile[0].id,
			img: itemImg,
			cb: cb
		});
	});

	// handle toggle refinement blocks
	$main.on('click', '.refinement h3', function() {
		$(this)
			.toggleClass('expanded')
			.siblings('ul')
			.toggle();
	});

	// handle events for updating grid
	$main.on(
		'click',
		'.refinements a, .pagination a, .breadcrumb-refinement-value a',
		function(e) {
			// don't intercept for category and folder refinements, as well as unselectable
			if (
				$(this).parents('.category-refinement').length > 0 ||
				$(this).parents('.folder-refinement').length > 0 ||
				$(this)
					.parent()
					.hasClass('unselectable')
			) {
				return;
			}
			e.preventDefault();
			updateProductListing(this.href, 'page-element');
		}
	);

	// handle events item click. append params.
	$main.on('click', '.product-tile a:not("#quickviewbutton")', function() {
		var a = $(this);
		// get current page refinement values
		var wl = window.location;

		var qsParams =
			wl.search.length > 1
				? util.getQueryStringParams(wl.search.substr(1))
				: {};
		var hashParams =
			wl.hash.length > 1
				? util.getQueryStringParams(wl.hash.substr(1))
				: {};

		// merge hash params with querystring params
		var params = $.extend(hashParams, qsParams);
		if (!params.start) {
			params.start = 0;
		}
		// get the index of the selected item and save as start parameter
		var tile = a.closest('.product-tile');
		var idx = tile.data('idx') ? +tile.data('idx') : 0;

		// convert params.start to integer and add index
		params.start = +params.start + (idx + 1);
		// set the hash and allow normal action to continue
		a[0].hash = $.param(params);
	});

	// handle sorting change
	$main
		.on('click', '#personnalisableCheckbox', function(e) {
			e.preventDefault();
			if ($(this).hasClass('selected')) {
				$(this).siblings('a[data-checkbox_value="false"]').trigger('click');
			} else {
				$(this).siblings('a[data-checkbox_value="true"]').trigger('click');
			}
		})
		.on('change', '.sort-by select', function(e) {
			e.preventDefault();
			updateProductListing(
			$(this).find('option:selected').val(),
			undefined,
			'#main #search-result-items'
			);
		}).on('change', '.items-per-page select', function() {
			var refineUrl = $(this)
				.find('option:selected')
				.val();
			if (refineUrl === 'INFINITE_SCROLL') {
				$('html')
					.addClass('infinite-scroll')
					.removeClass('disable-infinite-scroll');
			} else {
				$('html')
					.addClass('disable-infinite-scroll')
					.removeClass('infinite-scroll');
				updateProductListing(refineUrl);
			}
        }).on('click', function(){
        	$('#gridSort ul:visible, #viewButton ul:visible').hide()
        }).on('click', '#gridSort, #viewButton', function(e) {
        	e.stopPropagation();
        	$(this).find('ul').toggle();
        }).on('click', '#gridSort li', function(e){
        	var params = $(this).data('value');
        	updateProductListing(params, undefined, '#main #search-result-items')

        	e.stopPropagation();
        	$('#gridSort li.selected').removeClass('selected');
        	$(this).addClass('selected');
        	$('#gridSort #sortRule').text($(this).text())
        }).on('click', '#viewButton li', function(e) {
        	e.stopPropagation();
        	$('#viewButton li.selected').removeClass('selected');
        	$(this).addClass('selected')
        	$('.product-img').addClass('visually-hidden');
			$('.product-img.' + e.target.dataset.view).removeClass('visually-hidden');
			$(this).parent().toggle();
        })

    $main.on('click', 'button#viewMore', function() {
        var _this = this;
        var url = $(this).attr('data-href');
        url = util.appendParamsToUrl(url, {
            format: 'ajax',
            view : $('#viewButton li.selected').data('view')
        });
        $.ajax({
            method: 'GET',
            url: url
        }).done(function(response) {
            $(_this).parent().replaceWith($(response));
            displayProductTileImage();
        })
    });

    // Price refinement
    $main.on("change", "input.nouislider-min, input.nouislider-max", function() {

        var url = $(this).parent().find('.nouislider').attr('data-url');

        // appendParam does nothing if param exists, so remove it
        url = util.removeParamFromURL(url, 'pmin');
        url = util.removeParamFromURL(url, 'pmax');

        // Set price refinement values
        url = util.appendParamToURL(url, 'pmin', $(this).parent().find('input.nouislider-min').val());
        url = util.appendParamToURL(url, 'pmax', $(this).parent().find('input.nouislider-max').val());

        url = util.appendParamToURL(url, 'format', 'page-element');

        updateProductListing(url, false);
    });

    var $gridViewControll = $('.grid-view-controll'),
    	$normalGridButton = $('.normal-grid'),
    	$wideGrid = $('.wide-grid'),
    	$content = $('.search-result-content');

    $gridViewControll.click(function(e){
    	if (!e.target.classList.contains('selected') && e.target != this) {
    		$(this).find('.selected').toggleClass('selected');

    		e.target.classList.add('selected');

    		if (e.target.dataset.gridtype && e.target.dataset.gridtype == 'wide') {
    			$content.addClass('wide-tiles')
    		} else {
    			$content.removeClass('wide-tiles')
    		}
    	}
    })

	//back to top only for mobile
	if (window.innerWidth < 1110) {
		var slideUp = $('#slide-top-button');
		$(window).scroll(function() {
			if ($(this).scrollTop()) {
				slideUp.show();
			} else {
				slideUp.hide();
			}
		});

		slideUp.click(function(e) {
			window.scrollTo(0, 0)
		})
	}
}

function priceSliderRange(prices) {
    var ranges = {};
    var price;
    var min, max;
    var values = [];
    for (var i = 0; i < prices.length; i++) {
        price = prices[i];
        if(!min || min > price.min) {
            min = price.min;
        }
        if(!max || max < price.max) {
            max = price.max;
        }
        values.push(price.min);
        if(i == prices.length - 1) {
            values.push(price.max);
        }
    }
    var value;
    for(var j = 0; j < values.length; j++) {
        value = values[j];
        if(value == min) {
            ranges['min'] = value;
        } else if(value == max) {
            ranges['max'] = value;
        } else {
            ranges['' + parseInt( j / (values.length - 1) *  100 ) + '%'] = value;
        }
    }

    return ranges;
}

function initPriceSlider() {
    var $container = $('#main');

    $container.each(function(){
        var $slider    = $(this).find('.nouislider'),
            $min       = $(this).find('input.nouislider-min'),
            $max       = $(this).find('input.nouislider-max'),
            currentMin = $slider.attr('data-currentMin'),
            currentMax = $slider.attr('data-currentMax'),
            prices     = $slider.attr('data-ranges'),
            url        = $slider.attr('data-url'),
            ranges     = priceSliderRange(JSON.parse(prices)),
            startMin   = $min.length > 0 && isNaN(parseInt($min.val())) ? parseInt(ranges["min"]) : parseInt($min.val()),
            startMax   = $max.length > 0 && isNaN(parseInt($max.val())) ? parseInt(ranges["max"]) : parseInt($max.val()),
            options    = {
                start: [startMin, startMax],
                step: 5,
                range: ranges,
                snap: true,
                tooltips: [true, true],
                connect: true
            };

        if($slider[0].noUiSlider) {
            $slider[0].noUiSlider.destroy();
        }

        noUiSlider.create($slider[0], options);

        $slider[0].noUiSlider.on('update', function( values, handle ) {
            var value = values[handle];
            if ( handle ) {
                $max.val(value);
            } else {
                $min.val(value);
            }
        });

        $slider[0].noUiSlider.on('set', function( values, handle ) {
            var value = values[handle];
            if ( handle ) {
                $max.val(value).trigger('change');
            } else {
                $min.val(value).trigger('change');
            }
        });
    });
}

function displayProductTileImage_ajax($product, url) {
    $.ajax({
        type: 'GET',
        dataType: 'html',
        url: url,
        async: true,
        success: function(response) {
            $product.find('.thumb-link').append(response);
        }
    });
}

function displayProductTileImage() {
     var $products = $("[data-itemid]")
     for(var i = 0; i < $products.length; i++) {
        var $product = $($products[i]);
        var productId = $product.attr('data-itemid')
        for(var j = 0; j < $("#viewButton li:not(.selected)").length; j++) {
            var option = $("#viewButton li:not(.selected)")[j]
            var url = $(option).attr("data-url");
            url = util.appendParamToURL(url, 'pid', productId);
            displayProductTileImage_ajax($product, url);
        }
    }
}

/**
 * Hide and seek category description longer than 100 letters
 *
 */
function displayCategoryDescription() {
	var categoryDescription = $('.js-category-description');
	var fullCategoryDescription = categoryDescription.text();
	var categoryViewmore = $('.js-category-viewmore');

	if  (fullCategoryDescription.length <= 100) {
		categoryViewmore.hide();
	} else {
		var shortText = $.trim(fullCategoryDescription).substring(0, 100).split(" ").slice(0, -1).join(" ") + "...";
		categoryDescription.html(shortText);
		categoryViewmore.click(function() {
			categoryDescription.html(fullCategoryDescription);
			$(this).hide();
		});
	}
}

exports.init = function() {
	compareWidget.init();
	if (SitePreferences.LISTING_INFINITE_SCROLL) {
		$(window).on('scroll', infiniteScroll);
	}
	productTile.init();
    initializeEvents();
    displayProductTileImage();
	initPriceSlider();
	displayCategoryDescription();
};
