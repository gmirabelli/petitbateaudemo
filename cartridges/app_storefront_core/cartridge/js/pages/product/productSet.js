'use strict';

var ajax = require('../../ajax'),
	tooltip = require('../../tooltip'),
    Promise = require('promise'),
    page = require('../../page'),
	util = require('../../util');

module.exports = function() {
	var $addToCart = $('#add-to-cart'),
		$addAllToCart = $('#add-all-to-cart'),
		$productSetList = $('#product-set-list');

	var updateAddToCartButtons = function() {
		if ($productSetList.find('.add-to-cart[disabled]').length > 0) {
			$addAllToCart.attr('disabled', 'disabled');
			// product set does not have an add-to-cart button, but product bundle does
			$addToCart.attr('disabled', 'disabled');
		} else {
			$addAllToCart.removeAttr('disabled');
			$addToCart.removeAttr('disabled');
		}
	};

	if ($productSetList.length > 0) {
		updateAddToCartButtons();
	}
	// click on swatch for product set
	$productSetList.on('click', '.product-set-item .swatchanchor', function(e) {
		e.preventDefault();
		if (
			$(this)
				.parents('li')
				.hasClass('unselectable')
		) {
			return;
		}
		var url = Urls.getSetItem + this.search;
		var $container = $(this).closest('.product-set-item');
		var qty = $container
			.find('form input[name="Quantity"]')
			.first()
			.val();

		ajax.load({
			url: util.appendParamToURL(url, 'Quantity', isNaN(qty) ? '1' : qty),
			target: $container,
			callback: function() {
				updateAddToCartButtons();
				tooltip.init();
			}
		});
	});

    // Adds product set to wishlist, first collected all PIDs of variations
    $('.product-set a[data-action=wishlist]').click(function(e) {
        e.preventDefault();
        var url = setWishlistUrlParams(Urls.wishlistAddProductsPS);

        return Promise.resolve(
            $.ajax({
                type: 'GET',
                url: url
            })
        ).then(function(response) {
            if (response.success || (!response.success && response.redirectUrl)) {
                page.redirect(response.redirectUrl);
            } else {
                throw new Error(response.error);
            }
        });
    });

    // Collects all PIDs of variations and add it to url
    function setWishlistUrlParams(url) {
        var pidElements = $('.product-set-item input[name=pid]');

        pidElements.each(function(index) {
            url = util.appendParamToURL(url, 'pid' + index, $(this).val());
        });
        url = util.appendParamToURL(url, 'productsCount', pidElements.length);

        return url;
    }

    // Checks if all products of set are in wishlist and set wishlist button correct class
    function setWishlistButtonStatus() {
        var url = setWishlistUrlParams(Urls.isInWishListPS);

        return Promise.resolve(
            $.ajax({
                type: 'GET',
                url: url
            })
        ).then(function(response) {
            if (response.exists) {
                $('.product-set a[data-action=wishlist]').addClass('in-wishlist');
            }
        });
    };

    setWishlistButtonStatus();
};
