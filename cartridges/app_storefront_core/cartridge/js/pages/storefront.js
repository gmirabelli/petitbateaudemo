'use strict';
exports.init = function() {
	var mySwiper = new Swiper('.swiper-container', {
		pagination: {
			el: '.swiper-pagination'
		},
		loop: true
	});
};
