"use strict";

var util = require('./util')
var $header = $('header.main-header'),
	$html = $('html'),
	$wrapper = $('#wrapper'),
	$headerArea = $('#header-area'),
	$nav = $('#navigation'),
	$headerContainer = $('.header-container'),
	$ul1 = $('ul.level-1-ul'),
	$customcat = $('.custom-categories'),
	$navMobileHeader = $('.nav-mobile-header-lvl1'),
	$topbar = $('.top-bar'),
	headerTop = $headerArea.offset().top,
	scrollTop = 0

function closeNav() {
	scrollTop = parseInt($html.css('top'), 10);
	$html.removeClass('menu-active')
		.css({
			'top': 0,
			'overflow-x': 'hidden'
		})
	$('html,body').scrollTop(-scrollTop)
	$wrapper.removeAttr('style')
	$header.removeAttr('style')
	$nav.removeAttr('style')

	// let time for the close animation, then we reset
	window.setTimeout(function(){
		$ul1.removeAttr('style')
		$('.cat-active').removeClass('cat-active')
		$('.level-current').removeClass('level-current')
	}, 400);
}

/* move elements for mobile version of the nav */
function doMenuMobile() {
	$nav.insertBefore($header)
	$customcat.insertAfter($navMobileHeader)
	$topbar.wrap('<li class="level-1-li js-top-bar"></li>')
	$('.js-top-bar').appendTo($ul1)
	
	bindNavMobile() // bind actions
}

/* move elements for desktop version of the nav */
function undoMenuMobile() {
	$nav.appendTo($headerContainer)
	$customcat.appendTo($ul1)
	$topbar.prependTo($header)
	$('.js-top-bar').remove()
	
	bindNavDesktop() // bind actions
}

/* actions in mobile */
function bindNavMobile() {
	// main menu toggle
	$wrapper.on('click tap', '.menu-toggle, #js-overlay', function () {
		if (!$html.hasClass('menu-active')) {
			var fixed = document.getElementById('js-overlay'),
				wrapperWidth = $wrapper.outerWidth(),
				navWidth = 380
			scrollTop = $(window).scrollTop()
			fixed.addEventListener('touchmove', function(e) {
				e.preventDefault()
			}, false)
			$html.addClass('menu-active')
				.css('top', -scrollTop);
			if (wrapperWidth < navWidth) {
				navWidth = wrapperWidth
			}
			$nav.css({
				'left': 0,
				'width': navWidth + 'px'
			})
		} else {
			closeNav()
		}
	});

	// close mobile nav
	$wrapper.on('click tap', '.close-nav', function () {
		closeNav()
	});

	// actions on categories menu
	$nav.off('click tap', 'a.level-1-title.has-sub-menu, ul.level-1-ul button.back');
	$nav.on('click tap', 'a.level-1-title.has-sub-menu, ul.level-1-ul button.back', bindMobileNavLink);

	// expand/collapse language selector
	$wrapper.on('click tap', '.language .title', function () {
		$(this)
			.parents('.language').toggleClass('expanded')
			.find('.languages-list').slideToggle()
		$nav.animate({
			scrollTop: 2000
		}, 400, 'linear')
	});
}

function bindMobileNavLink(e) {
	var $el = $(this),
		$parent = $el.parents('li').eq(0),
		$currentEl = $('.level-current')
	if ($el.hasClass('back')) { // back to N1 level
		e.preventDefault()
		$ul1.css('left', '0')

		// let time for the animation before hiding the children (needed to calculate the scroll)
		window.setTimeout(function(){
			$currentEl.removeAttr('style')
		}, 400);
		
		$currentEl.removeClass('level-current')
		$parent
			.removeClass('cat-active')
			.parents('.cat-active').children('div.level-2').eq(0).addClass('level-current')
	} else { // open N2 level
		e.preventDefault()
		if (!$parent.hasClass('cat-active')) {
			$ul1.css('left', '-100%')
			$currentEl.removeClass('level-current')
			$('.cat-active').removeClass('cat-active')
			$parent
				.addClass('cat-active')
				.children('div.level-2').eq(0).addClass('level-current').show()
		}
	}
	checkHeightNav()
	$nav.animate({
		scrollTop: 0
	}, 400, 'linear')
}

function checkHeightNav() {
	// calculate the height for the mobile nav when browsing multilevels nav
	$('ul.level-1-ul, div.level-2').css({
		'height': 'auto',
		'overflow': 'visible'
	});
	var l1Height = $('ul.level-1-ul').outerHeight(),
		l2Height = $('.cat-active div.level-2').outerHeight(),
		scrollHeight = 0
	if (l2Height != null && l2Height < l1Height && $('.level-current').hasClass('level-2')) {
		scrollHeight = l2Height
	} else {
		scrollHeight = $('.cat-active:last > div').outerHeight()
	}
	if (scrollHeight != 0) {
		$('ul.level-1-ul, .cat-active div.level-2').css({
			'height': scrollHeight+'px',
			'overflow': 'hidden'
		});
	}
}

/* remove actions for mobile in desktop mode */
function bindNavDesktop() {
	$html.removeClass('menu-active')
	$wrapper
		.off('click tap', '.menu-toggle, #js-overlay')
		.removeAttr('style')
	$header.removeAttr('style')
	$nav
		.removeAttr('style')
		.off('click tap', 'a.level-1-title.has-sub-menu, ul.level-1-ul button.back', bindMobileNavLink)

	$nav.find('.cat-active').removeClass('cat-active')
	$nav.find('.level-current').removeAttr('style').removeClass('level-current')
	$('ul.level-1-ul, div.level-2').removeAttr('style')

	// expand/collapse language selector
	$wrapper.off('click tap', '.language .title')
}

// sticky header
function initStickyHeader() {
	setHeightHeader()
	$(window)
		.on('scroll', function() {
			if ($(window).scrollTop() > headerTop) {
				$header.addClass('sticky')
			} else {
				$header.removeClass('sticky')
				$('.header-search').find('input').blur()
			}
		}).on('resize', function(){
			setHeightHeader()
		})
}

// set the height for the sticky header
function setHeightHeader() {
	var sticky = false
	if ($header.hasClass('sticky')) {
		sticky = true
	}
	$header.removeClass('sticky')
	var headerHeight = $header.outerHeight()
	if (sticky) {
		$header.addClass('sticky')
	}
	$headerArea.css('height', headerHeight+'px')
}

/* switch display mode mobile/desktop */
function init() {
	$(document).on('desktopAction', function(){
		undoMenuMobile()
	});
	$(document).on('mobileAction', function(){
		doMenuMobile()
	});
	if (util.isMobile() == true) {
		$html.addClass('touch-device')
	}
	initStickyHeader()
}

exports.init = init
