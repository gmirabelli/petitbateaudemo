var countries = require('~/cartridge/countries');
var Locale = require('dw/util/Locale');
var CustomObjectMgr = require('dw/object/CustomObjectMgr');
var Logger = require('dw/system/Logger');
var URLUtils = require('dw/web/URLUtils');

var countryLocalesKeyName = 'CountryLocales';

/**
 * @description filter out the countries array to return only ones that are allowed in
 * site's allowedLocales
 * @return {array} allowedCountries array of countries that have allowed locales
 */
function getCountries() {
	var site = dw.system.Site.getCurrent();
	var allowedLocales = site.getAllowedLocales();
	var allowedCountries = countries.filter(function (country) {
		var hasAllowedLocale = false;
		// loop over allowed locales
		for (var i = 0; i < allowedLocales.length; i++) {
			var locale = Locale.getLocale(allowedLocales[i]);
			if (country.countryCode === locale.country) {
				hasAllowedLocale = true;
				break;
			}
		}
		return hasAllowedLocale;
	});
	return allowedCountries;
}

function getCountriesGroupedBy(group) {
	var countries = getCountries();
	var countriesGrouped = {};
	countries.forEach(function (country) {
		var key = country.hasOwnProperty(group) ? country[group] : undefined;
		if (countriesGrouped.hasOwnProperty(key)) {
			countriesGrouped[key].push(country);
		} else {
			countriesGrouped[key] = [country];
		}
	});
	return countriesGrouped;
}

/**
 * @description iterate over the countries array, find the first country that has the current locale
 * @param {PipelineDictionary} pdict the current pdict object
 * @return {object} country the object containing the country's settings
 */
function getCurrent(pdict) {
	if (!countries || countries.length === 0) {
		return;
	}
	var currentLocale = Locale.getLocale(pdict.CurrentRequest.locale);
	var country;
	if (!currentLocale.country) {
		return countries[0]; // return the first in the list if the requested one is not available
	}
	for (var i = 0; i < countries.length; i++) {
		var _country = countries[i];
		if (_country.countryCode === currentLocale.country) {
			country = _country;
			break;
		}
	}
	return country || countries[0];  // return the first in the list if the requested one is not available
}

/**
 * @description Gets all custom objects of type "CountryLocales" and sort them by "custom.position" attribute (ascending mode)
 * @return {SeekableIterator} custom objects
 */
function getCountriesFromCO() {
    var countries;
    try {
        countries = CustomObjectMgr.queryCustomObjects(countryLocalesKeyName, '', 'custom.position asc'); // Sorting objects by "custom.position" attribute
    } catch (e) {
        Logger.warn('Could not get CountryLocales from CO: ', e.message);
    }

    return countries;
}

/**
 * @description Gets country Custom Object by provided code
 * @param {String} countryCode key value for Custom Object
 * @return {mixed} valueToReturn null or object with attributes
 */
function getCountryFromCOByCode(countryCode) {
    var countryObj;
    countryCode = countryCode ? countryCode.toUpperCase() : '';
    try {
        countryObj = CustomObjectMgr.getCustomObject(countryLocalesKeyName, countryCode);
    } catch (e) {
        Logger.warn('Could not get country CO by code: ', e.message);
    }

    return countryObj;
}

exports.getCountries = getCountries;
exports.getCountriesGroupedBy = getCountriesGroupedBy;
exports.getCurrent = getCurrent;
exports.getCountriesFromCO = getCountriesFromCO;
exports.getCountryFromCOByCode = getCountryFromCOByCode;
