/**
 * ©2013-2018 salesforce.com, inc. All rights reserved.
 * 
 * Apply a shipping price override to a basket
 * 
 */

importScript("int_ocapi_ext_core:api/EAOverride.ds");
importScript("int_ocapi_ext_core:api/EAStatus.ds");
importScript("int_ocapi_ext_core:api/Authorize.ds");

var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

/**
 * setShippingPriceOverride
 * 
 * called to set a shipping price override
 * 
 * @param basket
 * @param shippingMethod
 * @return status
 */
exports.setShippingPriceOverride = function(basket, shippingMethod) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("shippingPriceOverride setShippingPriceOverride start");
    var patch = shippingMethod.c_patchInfo;
    if (patch) {
        var type = patch.type;
        var data = patch.data;
        // for now, all this hook is used for is for applying a shipping price
        // override
        if (type === "shipping_price_override") {
            var d = {
                price_override_type : data.price_override_type,
                price_override_reason_code : data.price_override_reason_code,
                price_override_value : data.price_override_value,
                manager_employee_id : data.manager_employee_id,
                manager_employee_passcode : data.manager_employee_passcode,
                manager_allowLOBO : data.manager_allowLOBO,
                kiosk_mode : data.kiosk_mode,
                ignore_permissions : data.ignore_permissions,
                employee_id : data.employee_id,
                employee_passcode : data.employee_passcode,
                store_id : data.store_id,
                Basket : basket
            };
            return applyShippingPriceOverride(d);
        }
    }
    log.info("shippingPriceOverride setShippingPriceOverride end");
    return new Status(Status.OK);
};

/**
 * applyShippingPriceOverride
 * 
 * apply the override
 * 
 * @param args
 * @return status
 */
function applyShippingPriceOverride(args) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("shippingPriceOverride applyShippingPriceOverride: entering script");
    var basket = args.Basket;
    var eaStatus = new EAStatus();

    if (empty(basket)) {
        eaStatus.findMessage("EA_EMP_AUTH_4001");
        args.ErrorJson = eaStatus.stringify();
        log.info("shippingPriceOverride applyShippingPriceOverride: exiting script with error EA_EMP_AUTH_4001");
        return new Status(Status.ERROR, "shippingOverrideError", eaStatus.stringify());
    }

    var agentAuthorize = new Authorize();
    agentAuthorize.authorize(args.employee_id, args.employee_passcode, args.store_id);
    // Check if the agent is logged in and if agent has log in on behalf of
    // permissions.
    var allowLOBO = agentAuthorize.allowLOBO;
    // if agent does not have permission, then check if logged in manager does
    if (!allowLOBO) {
        allowLOBO = args.manager_allowLOBO;
    }
    // if the client says to ignore the permissions for setting this override,
    // ignore them
    if (args.ignore_permissions) {
        allowLOBO = true;
    }
    if (!allowLOBO) {
        // if no permission, then return error
        eaStatus.findMessage("EA_EMP_AUTH_4001");
        args.ErrorJson = eaStatus.stringify();
        log.info("shippingPriceOverride applyShippingPriceOverride: exiting script with error EA_EMP_AUTH_4001");
        return new Status(Status.ERROR, "shippingOverridesError", eaStatus.stringify());
    } else {
        // if permission, then proceed
        log.info("shippingPriceOverride applyShippingPriceOverride: associate {0} is applying a shipping override", args.employee_id);

        var eaOverride = new EAOverride();
        var basketUpdate = false;

        // if the override type is 'none', it is being removed and
        // authorizations do not have to be checked
        if (args.price_override_type.equalsIgnoreCase("None")) {
            basketUpdate = true;
        } else {
            // Check if manager's credentials are passed for override
            if (!empty(args.manager_employee_id)) {

                // fetch Manager's permissions for override and update basket
                var authorize = new Authorize();
                authorize.authorize(args.manager_employee_id, args.manager_employee_passcode, args.store_id);

                if (authorize.isAuthorized()) {
                    eaOverride.verifyAuthorizationForShipping(authorize.associateInfo.permissionGroupId, dw.order.ShippingMgr.getShippingCost(basket.defaultShipment.getShippingMethod(),
                            basket.totalGrossPrice).value, args.price_override_type, args.price_override_value);
                    basketUpdate = eaOverride.allowBasketUpdate;
                }

            } else {
                if (!args.ignore_permissions) {
                    // fetch Agent's permissions for override and update basket
                    eaOverride.verifyAuthorizationForShipping(agentAuthorize.associateInfo.permissionGroupId, dw.order.ShippingMgr.getShippingCost(basket.defaultShipment.getShippingMethod(),
                            basket.totalGrossPrice).value, args.price_override_type, args.price_override_value);
                    basketUpdate = eaOverride.allowBasketUpdate;
                } else {
                    // request says ignore the permissions, so just go ahead
                    basketUpdate = true;
                }
            }
        }

        // Saved Override details in Shipping Line item custom attributes
        // iterate to find shipping line item of the basket and set price
        var allLineItems = basket.getAllLineItems().iterator();
        while (allLineItems.hasNext()) {
            var lineItem = allLineItems.next();
            if (lineItem.describe().ID.equalsIgnoreCase("ShippingLineItem")) {
            	// don't override surcharge shipping charges
                if ("surcharge" in lineItem && lineItem.surcharge) {
                    continue;
                }
                var obj = lineItem.custom.eaCustomAttributes ? JSON.parse(lineItem.custom.eaCustomAttributes) : {};
                obj.eaManagerEmployeeId = "";
                obj.eaPriceOverrideType = "";
                obj.eaPriceOverrideValue = "";
                obj.eaOverrideReasonCode = "";
                lineItem.custom.eaManagerEmployeeId = "";
                lineItem.custom.eaPriceOverrideType = "";
                lineItem.custom.eaPriceOverrideValue = "";
                lineItem.custom.eaOverrideReasonCode = "";
                if (basketUpdate) {
                    lineItem.custom.eaEmployeeId = args.employee_id;
                    if (args.manager_employee_id) {
                        lineItem.custom.eaManagerEmployeeId = args.manager_employee_id;
                    }
                    lineItem.custom.eaPriceOverrideType = args.price_override_type;
                    lineItem.custom.eaPriceOverrideValue = args.price_override_value;
                    lineItem.custom.eaOverrideReasonCode = args.price_override_reason_code;

                    obj.eaEmployeeId = args.employee_id;
                    obj.eaManagerEmployeeId = args.manager_employee_id;
                    obj.eaPriceOverrideType = args.price_override_type;
                    obj.eaPriceOverrideValue = args.price_override_value;
                    obj.eaOverrideReasonCode = args.price_override_reason_code;
                    if (obj.eaPriceOverrideType === "none") {
                        delete obj.eaManagerEmployeeId;
                        delete obj.eaPriceOverrideType;
                        delete obj.eaPriceOverrideValue;
                        delete obj.eaOverrideReasonCode;
                    }
                    lineItem.custom.eaCustomAttributes = JSON.stringify(obj);

                } else {

                    eaStatus.findMessage(eaOverride.eaStatusCode);
                    args.ErrorJson = eaStatus.stringify();
                }
            }
        }
    }
    log.info("shippingPriceOverride applyShippingPriceOverride: exiting script");
    return new Status(Status.OK);
};