/**
 * ©2013-2018 salesforce.com, inc. All rights reserved.
 * 
 * basket_hook_scripts.js
 * 
 * Handles OCAPI hooks for basket calls
 */

var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');
var LineItemCtnr = require('dw/order/LineItemCtnr');
var StoreMgr = require('dw/catalog/StoreMgr');
var PriceBookMgr = require('dw/catalog/PriceBookMgr');

var updateProductLineItem = require('./updateProductLineItem');
var basketPatch = require('./basketPatch');
var validateAddress = require('./validateAddress');
var shippingPriceOverride = require('./shippingPriceOverride');

/**
 * setPriceBook - it sets the price book for a particular country defined in
 * custom preferences -> EACatalog -> Country Configuration
 * 
 */
function setPriceBook() {
    var req = request.httpParameters;
    // only call this code for Endless Aisle
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        var country = req.country[0];
        var sitePrefs = JSON.parse(dw.system.Site.getCurrent().getPreferences().custom.eaCountryConfiguration)[country];
        var salePriceBook = PriceBookMgr.getPriceBook(sitePrefs.sale_price_book);
        var listPriceBook = PriceBookMgr.getPriceBook(sitePrefs.list_price_book);
        var setPriceBook = PriceBookMgr.setApplicablePriceBooks(salePriceBook, listPriceBook);
    }
};

/**
 * the afterPOST hook - called after adding an item to the basket
 */
exports.afterPOST = function(basket, productItems) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info(" basket afterPOST: entering script");
    var req = request.httpParameters;
    // only call this code for Endless Aisle
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        for ( i = 0; i < productItems.length; i++) {
            var productItem = productItems[i];
            // need to set the channel type to DSS and add some custom
            // attributes to the basket
            basket.setChannelType(LineItemCtnr.CHANNEL_TYPE_DSS);
            basket.custom.eaEmployeeId = productItem.c_employee_id;
            basket.custom.eaStoreId = productItem.c_store_id;
            // now update the product item
            var status = updateProductLineItem.updateProductItem(basket, productItem, productItem.c_employee_id, productItem.c_store_id);
            if (status.status == Status.ERROR) {
                log.info("basket afterPOST: error on updateProductLineItem");
                return status;
            }
        }
        // now call calculate again since the options on an item may have
        // changed and the basket needs recalculating
        dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
    }
    log.info("basket afterPOST: exiting script");
    return new Status(Status.OK);
};

/**
 * the beforePOST basket hook. This gets called before a basket post operation.
 */
exports.beforePOST = function() {
    setPriceBook();
}

/**
 * the beforePatch basket hook. This gets called before a basket patch
 * operation.
 */
exports.beforePatch = function(basket, update) {
    setPriceBook();
    var log = Logger.getLogger("instore-audit-trail");
    // only call this code for Endless Aisle
    var req = request.httpParameters;
    if (update.c_patchInfo && req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        log.info("basket beforePatch: entering script");
        // call the patch script. if that returns an error, return that error
        var status = basketPatch.beforePatch(basket, update);
        if (status.status == Status.ERROR) {
            log.info("basket beforePatch: error on update");
            return status;
        }
        // make sure to call the calculate hook
        dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
        log.info("basket beforePatch: exiting script");
    }
    return new Status(Status.OK);
};

/**
 * the afterPatch basket hook. This gets called after a basket patch operation.
 */
exports.afterPatch = function(basket, update) {
    var log = Logger.getLogger("instore-audit-trail");
    var req = request.httpParameters;
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        log.info("basket afterPatch: entering script");
        // call the patch script. if that returns an error, return that error
        var status = basketPatch.afterPatch(basket, update);
        if (status.status == Status.ERROR) {
            log.info("basket afterPatch: error on update");
            return status;
        }
        log.info("basket afterPatch: exiting script");
    }
    return new Status(Status.OK);
};

/**
 * the afterPatchItem basket hook. This gets called after updating a product
 * line item in the basket
 */
exports.afterPatchItem = function(basket, productItem) {
    var log = Logger.getLogger("instore-audit-trail");
    var req = request.httpParameters;
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        log.info("basket afterPatchItem: entering script");
        // call the patch script. if that returns an error, return that error
        var status = updateProductLineItem.updateProductItem(basket, productItem, productItem.c_employee_id);
        if (status.status == Status.ERROR) {
            log.info("basket afterPatchItem: error on update");
            return status;
        }
        dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
        log.info("basket afterPatchItem: exiting script");
    }
    return new Status(Status.OK);
};

/**
 * the validateBasketAddress to validate the customer address
 */
function validateBasketAddress(basket, address) {
    var log = Logger.getLogger("instore-audit-trail");
    var status = new Status(Status.OK);
    basket.setChannelType(LineItemCtnr.CHANNEL_TYPE_DSS);
    log.info("validateBasketAddress: before call to validate");
    // call the customization script
    status = validateAddress.validateBasketAddress(address);
    log.info("validateBasketAddress: after call to validate");
    return status;
}

/**
 * the beforePutShippingAddress hook. This is used to validate a shipping
 * address.
 */
exports.beforePutShippingAddress = function(basket, shipment, orderAddress) {
    setPriceBook();
    var req = request.httpParameters;
    // only call this code for Endless Aisle
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        return validateBasketAddress(basket, orderAddress);
    }
};

/**
 * the beforePutBillingAddress hook. This is used to validate a billing address.
 */
exports.beforePutBillingAddress = function(basket, orderAddress) {
    setPriceBook();
    var req = request.httpParameters;
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        return validateBasketAddress(basket, orderAddress);
    }
};

/**
 * the beforePutShippingMethod hook. This is used to add a shipping price
 * override.
 */
exports.beforePutShippingMethod = function(basket, shipment, shippingMethod) {
    setPriceBook();
    var log = Logger.getLogger("instore-audit-trail");
    // only call this code for Endless Aisle
    var req = request.httpParameters;
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        log.info("basket beforePutShippingMethod: entering script");
        // call the customization script
        var status = shippingPriceOverride.setShippingPriceOverride(basket, shippingMethod);
        // if that has an error, return error
        if (status.status == Status.ERROR) {
            log.info("basket beforePutShippingMethod: error on setShippingPriceOverride");
            return status;
        }
        // make sure to call the calculate hook
        dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
        log.info("basket beforePutShippingMethod: exiting script");
    }

    return new Status(Status.OK);
};

/**
 * This function sets the store inventory id on each line it for the store it is
 * going to be picked up at
 */
function setStoreInventoryIdOnLineItems(pickupStoreId, basket) {
    var log = Logger.getLogger("instore-audit-trail");
    var inventoryListID = null;
    log.info("basket setStoreInventoryIdOnLineItems: entering script " + pickupStoreId);
    if (!basket) {
        return Status.ERROR;
    }

    if (pickupStoreId != null) {
        var pickupStore = StoreMgr.getStore(pickupStoreId);
        if (!pickupStore) {
            return Status.ERROR;
        }
        inventoryListID = pickupStore.getInventoryListID();
        if (!inventoryListID) {
            return Status.ERROR;
        }
    }

    for each (var item in basket.getAllProductLineItems()) {
        log.info("basket setStoreInventoryIdOnLineItems: setting inventory id " + inventoryListID);
        item.setProductInventoryListID(inventoryListID);
    }
    log.info("basket setStoreInventoryIdOnLineItems: exiting script");
    return Status.OK;
}

/**
 * the afterPutShippingMethod hook. This is used to add a additional data to the
 * shipment.
 */
exports.afterPutShippingMethod = function(basket, shipment, shippingMethod) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("basket afterPutShippingMethod: entering script");
    // only call this code for Endless aisle
    var req = request.httpParameters;
    if (req.c_endlessaisle && req.c_endlessaisle[0] == "true") {
        if (shippingMethod.c_isDifferentStorePickup) {
            log.info("basket afterPutShippingMethod: different store pickup selected");
            if (shippingMethod.c_pickupFromStoreId) {
                // clear out any shipping overrides
                var status = shippingPriceOverride.setShippingPriceOverride(basket, {
                    c_patchInfo : {
                        type : "shipping_price_override",
                        data : {
                            price_override_type : "none",
                            ignore_permissions : true
                        }
                    }
                });
                // if that has an error, return error
                if (status.status == Status.ERROR) {
                    log.info("basket afterPutShippingMethod: unable to clear override with setShippingPriceOverride");
                    return status;
                }

                shipment.custom.fromStoreId = shippingMethod.c_pickupFromStoreId;
                var status = setStoreInventoryIdOnLineItems(shippingMethod.c_pickupFromStoreId, basket);
                if (status.status == Status.ERROR) {
                    log.info('basket afterPutShippingMethod: Unable to set inventory list id into product line items');
                    return status;
                }
            }
            if (shippingMethod.c_message) {
                shipment.custom.storePickupMessage = shippingMethod.c_message;
            }
        } else {
            log.info("basket afterPutShippingMethod: not different store pickup");
            var status = setStoreInventoryIdOnLineItems(null, basket);
            if (status.status == Status.ERROR) {
                log.info('basket afterPutShippingMethod: Unable to clear inventory list id from product line items');
                return status;
            }
        }
    }
    dw.system.HookMgr.callHook("dw.ocapi.shop.basket.calculate", "calculate", basket);
    log.info("basket afterPutShippingMethod: exiting script");
    return new Status(Status.OK);
};

/**
 * the validateBasket hook. EA doesn't require a payment instrument when
 * submitting the basket to create an order, so remove that flash error
 */
exports.validateBasket = function(basketResponse, checkForSubmit) {
    var log = Logger.getLogger("instore-audit-trail");
    if (request && checkForSubmit && basketResponse.channelType == "dss") {
        log.info("validateBasket: remove payment method required flash and invalid coupon status flash");
        var flashToRemove = [];
        for each (flash in basketResponse.flashes) {
            if (flash.type == "PaymentMethodRequired" || flash.type == "InvalidCouponStatus") {
                flashToRemove.push(flash);
            }
        }
        // now remove the flashes if any
        for each (flash in flashToRemove) {
            basketResponse.removeFlash(flash);
        }
    }
    return new Status(Status.OK);
};

/**
 * the beforePostCoupon hook. This is used to add a coupon.
 */
exports.beforePostCoupon = function() {
    setPriceBook();
};

/**
 * the beforePatchItem hook. This is used to update an item in the basket.
 */
exports.beforePatchItem = function() {
    setPriceBook();
};

/**
 * the beforeDeleteItem hook. This is used to delete an item in the basket.
 */
exports.beforeDeleteItem = function() {
    setPriceBook();
};

/**
 * the beforeDeleteCoupon hook. This is used to delete a coupon from the basket.
 */
exports.beforeDeleteCoupon = function() {
    setPriceBook();
};

/**
 * the beforePutCustomer hook
 */
exports.beforePutCustomer = function() {
    setPriceBook();
};

exports.setPriceBook = setPriceBook;
