/**
 * ©2013-2018 salesforce.com, inc. All rights reserved.
 * 
 * Check authorizations for product price override Save Product Price Override in the Product Line item Update basket
 */

importScript("int_ocapi_ext_core:api/EAOverride.ds");
importScript("int_ocapi_ext_core:api/EAStatus.ds");
importScript("int_ocapi_ext_core:api/Authorize.ds");

var Logger = require('dw/system/Logger');
var Status = require('dw/system/Status');

/**
 * applyProductPriceOverride
 * 
 * apply a product price override
 */
exports.applyProductPriceOverride = function(args) {
    var log = Logger.getLogger("instore-audit-trail");
    log.info("productPriceOverride applyProductPriceOverride: entering script");

    var basket = args.Basket;
    var eaStatus = new EAStatus();

    if (empty(basket)) {
        eaStatus.findMessage("EA_EMP_AUTH_4001");
        args.ErrorJson = eaStatus.stringify();
        log.info("productPriceOverride applyProductPriceOverride: exiting script with EA_EMP_AUTH_4001 error");
        return new Status(Status.ERROR, "productOverrideError", eaStatus.stringify());
    }

    var agentAuthorize = new Authorize();
    agentAuthorize.authorize(args.employee_id, args.employee_passcode, args.store_id);

    // Check if the agent is logged in and if the agent has the log in on behalf
    // of permission.
    var allowLOBO = agentAuthorize.allowLOBO;
    if (!allowLOBO) {
        // if the agent does not have LOBO permission, then see if the logged in
        // manager does
        allowLOBO = args.manager_allowLOBO;
    }
    if (!allowLOBO && !args.price_override_type.equalsIgnoreCase("None")) {
        // if no LOBO permission, then return error, but if removing overrides,
        // just proceed
        eaStatus.findMessage("EA_EMP_AUTH_4001");
        args.ErrorJson = eaStatus.stringify();
        log.info("productPriceOverride applyProductPriceOverride: exiting script with EA_EMP_AUTH_4001 error");
        return new Status(Status.ERROR, "productOverrideError", eaStatus.stringify());
    } else {
        // if has LOBO permission, then proceed
        log.info("productPriceOverride applyProductPriceOverride: associate " + args.employee_id + " is creating a product price override of " + args.price_override_value + " on product "
                + args.product_id);
        var eaOverride = new EAOverride();
        var basketUpdate = false;

        // if the override type is 'none', it is being removed and
        // authorizations do not have to be checked
        if (args.price_override_type.equalsIgnoreCase("None")) {
            basketUpdate = true;
        } else {

            // Check if manager's credentials are passed for override
            if (!empty(args.manager_employee_id)) {

                // fetch Manager's permissions for override and update basket
                var authorize = new Authorize();
                authorize.authorize(args.manager_employee_id, args.manager_employee_passcode, args.store_id);

                if (authorize.isAuthorized()) {
                    eaOverride.verifyAuthorization(authorize.associateInfo.permissionGroupId, args.product_id, args.price_override_type, args.price_override_value);
                    basketUpdate = eaOverride.allowBasketUpdate;
                }

            } else {

                // fetch Agent's permissions for override and update basket
                eaOverride.verifyAuthorization(agentAuthorize.associateInfo.permissionGroupId, args.product_id, args.price_override_type, args.price_override_value);
                basketUpdate = eaOverride.allowBasketUpdate;
            }
        }

        // Saved Override details in Product Line item custom attributes

        var productLineItems = basket.getProductLineItems().iterator().asList();
        if (productLineItems.size() < args.index) {
            return new Status(Status.ERROR);
        }
        var productLineItem = productLineItems.get(args.index);

        if (productLineItem.productID.equalsIgnoreCase(args.product_id) &&  productLineItem.UUID.equalsIgnoreCase(args.lineItem_id)) {
            var obj = productLineItem.custom.eaCustomAttributes ? JSON.parse(productLineItem.custom.eaCustomAttributes) : {};
            obj.eaManagerEmployeeId = "";
            obj.eaprice_override_type = "";
            obj.eaPriceOverrideValue = "";
            obj.eaOverrideReasonCode = "";
            productLineItem.custom.eaManagerEmployeeId = "";
            productLineItem.custom.eaPriceOverrideType = "";
            productLineItem.custom.eaPriceOverrideValue = "";
            productLineItem.custom.eaOverrideReasonCode = "";
            if (basketUpdate) {
                productLineItem.custom.eaEmployeeId = args.employee_id;
                if (args.manager_employee_id) {
                    productLineItem.custom.eaManagerEmployeeId = args.manager_employee_id;
                }
                productLineItem.custom.eaPriceOverrideType = args.price_override_type;
                productLineItem.custom.eaPriceOverrideValue = args.price_override_value;
                productLineItem.custom.eaOverrideReasonCode = args.price_override_reason_code;

                obj.eaEmployeeId = args.employee_id;
                obj.eaManagerEmployeeId = args.manager_employee_id;
                obj.eaPriceOverrideType = args.price_override_type;
                obj.eaPriceOverrideValue = args.price_override_value;
                obj.eaOverrideReasonCode = args.price_override_reason_code;
                if (obj.eaPriceOverrideType === "none") {
                    delete obj.eaManagerEmployeeId;
                    delete obj.eaPriceOverrideType;
                    delete obj.eaPriceOverrideValue;
                    delete obj.eaOverrideReasonCode;
                }
                productLineItem.custom.eaCustomAttributes = JSON.stringify(obj);
            } else {

                eaStatus.findMessage(eaOverride.eaStatusCode);
                args.ErrorJson = eaStatus.stringify();
            }
        }
    }

    log.info("productPriceOverride applyProductPriceOverride: exiting script without error");
    return new Status(Status.OK);
};